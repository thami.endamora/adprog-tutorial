package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    private String name;
    private String role;
    List<Member> members = new ArrayList<>();

    public PremiumMember(String name,String role){
        this.name = name;
        this.role = role;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        if (this.role.equals("Master")) {
            this.members.add(member);
        } else {
            if (this.members.size() < 3) {
                this.members.add(member);
            }
        }
    }

    @Override
    public void removeChildMember(Member member) {
        this.members.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return this.members;
    }
}
