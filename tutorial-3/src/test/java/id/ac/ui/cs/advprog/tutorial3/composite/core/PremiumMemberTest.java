package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        member.addChildMember(new OrdinaryMember("Thami","Student"));
        assertEquals(1,member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        OrdinaryMember child = new OrdinaryMember("Thami","Student");
        member.addChildMember(child);
        assertEquals(1,member.getChildMembers().size());
        member.removeChildMember(child);
        assertEquals(0,member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        member.addChildMember(new OrdinaryMember("First","Student"));
        member.addChildMember(new OrdinaryMember("Second","Student"));
        member.addChildMember(new OrdinaryMember("Third","Student"));
        member.addChildMember(new OrdinaryMember("Fourth","Student"));
        assertEquals(3,member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        PremiumMember master = new PremiumMember("Thami", "Master");
        master.addChildMember(new OrdinaryMember("First","Student"));
        master.addChildMember(new OrdinaryMember("Second","Student"));
        master.addChildMember(new OrdinaryMember("Third","Student"));
        master.addChildMember(new OrdinaryMember("Fourth","Student"));
        assertEquals(4,master.getChildMembers().size());
    }
}
