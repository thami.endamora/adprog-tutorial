package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.WeaponProducer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;



public class EnhancerDecoratorTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;
    Weapon weapon5;
    @Test
    public void testAddWeaponEnhancement(){
        weapon1 = WeaponProducer.WEAPON_GUN.createWeaponEnhancer();
        weapon2 = WeaponProducer.WEAPON_LONGBOW.createWeaponEnhancer();
        weapon3 = WeaponProducer.WEAPON_GUN.createWeaponEnhancer();
        weapon4 = WeaponProducer.WEAPON_SHIELD.createWeaponEnhancer();
        weapon5 = WeaponProducer.WEAPON_SWORD.createWeaponEnhancer();
        int one = weapon1.getWeaponValue();
        int two = weapon2.getWeaponValue();
        int three = weapon3.getWeaponValue();
        int four = weapon4.getWeaponValue();
        int five = weapon5.getWeaponValue();
        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon5);

        assertEquals(one + 50,weapon1.getWeaponValue());
        assertEquals(two + 15,weapon2.getWeaponValue());
        assertEquals(three + 1,weapon3.getWeaponValue());
        assertEquals(four + 5,weapon4.getWeaponValue());
        assertEquals(five + 10,weapon5.getWeaponValue());
    }

}
