package id.ac.ui.cs.advprog.tutorial5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tutorial4Application {

    public static void main(String[] args) {
        SpringApplication.run(Tutorial4Application.class, args);
    }

}
