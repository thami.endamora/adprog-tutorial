package id.ac.ui.cs.advprog.tutorial5.abstractfactory.controller;

import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.service.AcademyService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping(path = "/academy")
public class AcademyController {

    private final AcademyService academyService;

    public AcademyController(AcademyService academyService) {
        this.academyService = academyService;
    }

    @GetMapping(path = "/")
    public String academyHome(Model model) {
        List<KnightAcademy> knightAcademies = academyService.getKnightAcademies();
        Knight knight = academyService.getKnight();
        model.addAttribute("academies", knightAcademies);
        model.addAttribute("knight", knight);
        return "abstractfactory/knight-academy";
    }

    @PostMapping(path = "/produce")
    public String produceKnight(@RequestParam(value = "academyName")
                                        String academyName,
                                @RequestParam(value = "knightType")
                                    String knightType,
                                Model model) {

        academyService.produceKnight(academyName, knightType);
        return "redirect:/academy/";
    }
}
