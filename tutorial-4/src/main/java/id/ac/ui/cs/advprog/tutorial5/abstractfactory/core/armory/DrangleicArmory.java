package id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory;

import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.armor.ShiningArmor;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.skill.ShiningForce;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.weapon.ShiningBuster;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.weapon.Weapon;

public class DrangleicArmory implements Armory {

    @Override
    public Armor craftArmor() {
        return new ShiningArmor();
    }

    @Override
    public Weapon craftWeapon() {
        return new ShiningBuster();
    }

    @Override
    public Skill learnSkill() {
        return new ShiningForce();
    }
}
