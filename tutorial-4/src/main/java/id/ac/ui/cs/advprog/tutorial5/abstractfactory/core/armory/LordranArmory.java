package id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory;

import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.armor.MetalArmor;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.weapon.ThousandJacker;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.weapon.Weapon;

public class LordranArmory implements Armory {

    @Override
    public Armor craftArmor() {
        return new MetalArmor();
    }

    @Override
    public Weapon craftWeapon() {
        return new ThousandJacker();
    }

    @Override
    public Skill learnSkill() {
        return new ThousandYearsOfPain();
    }
}
