package id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.armor;

public interface Armor {

    String getName();
    String getDescription();
}
