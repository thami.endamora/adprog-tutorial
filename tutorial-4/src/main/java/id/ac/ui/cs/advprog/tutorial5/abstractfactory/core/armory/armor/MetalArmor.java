package id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.armor;

public class MetalArmor implements Armor {

    @Override
    public String getName() {
        return "Metal Armor";
    }

    @Override
    public String getDescription() {
        return "Metal!";
    }
}
