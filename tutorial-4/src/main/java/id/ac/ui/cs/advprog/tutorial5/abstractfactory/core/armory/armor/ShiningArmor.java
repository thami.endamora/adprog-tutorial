package id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.armor;

public class ShiningArmor implements Armor {

    @Override
    public String getName() {
        return "Shining Armor";
    }

    @Override
    public String getDescription() {
        return "Shine!";
    }
}
