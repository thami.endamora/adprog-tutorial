package id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.skill;

public interface Skill {

    String getName();
    String getDescription();
}
