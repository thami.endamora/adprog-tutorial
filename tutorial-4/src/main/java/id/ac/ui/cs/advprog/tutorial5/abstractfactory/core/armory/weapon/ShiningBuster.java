package id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.weapon;

public class ShiningBuster implements Weapon {

    @Override
    public String getName() {
        return "Shining Buster";
    }

    @Override
    public String getDescription() {
        return "Shine!";
    }
}
