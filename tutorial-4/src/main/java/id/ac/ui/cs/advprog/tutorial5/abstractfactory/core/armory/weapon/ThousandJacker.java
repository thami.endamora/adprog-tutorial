package id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.weapon;

public class ThousandJacker implements Weapon {

    @Override
    public String getName() {
        return "Thousand Jacker";
    }

    @Override
    public String getDescription() {
        return "Jacker!";
    }
}
