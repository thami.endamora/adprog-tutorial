package id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.weapon;

public interface Weapon {

    String getName();
    String getDescription();
}
