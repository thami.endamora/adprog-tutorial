package id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.Armory;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.weapon.Weapon;

public abstract class Knight {

    protected Armory armory;
    protected String name;
    protected Armor armor;
    protected Weapon weapon;
    protected Skill skill;

    public abstract void prepare();

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Armor getArmor() {
        return armor;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public Skill getSkill() {
        return skill;
    }
}
