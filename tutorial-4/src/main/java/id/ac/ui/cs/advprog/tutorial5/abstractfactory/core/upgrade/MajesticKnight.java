package id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.Armory;

public class MajesticKnight extends Knight {

    public MajesticKnight(Armory armory) {
        this.armory = armory;
    }

    @Override
    public void prepare() {
        this.name = "Majestic Knight";
        this.armor = this.armory.craftArmor();
        this.weapon = this.armory.craftWeapon();
    }
}
