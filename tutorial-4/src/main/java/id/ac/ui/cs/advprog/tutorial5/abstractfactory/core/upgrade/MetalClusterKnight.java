package id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.Armory;

public class MetalClusterKnight extends Knight {

    public MetalClusterKnight(Armory armory) {
        this.armory = armory;
    }

    @Override
    public void prepare() {
        this.name = "Metal Cluster";
        this.armor = this.armory.craftArmor();
        this.skill = this.armory.learnSkill();
    }
}
