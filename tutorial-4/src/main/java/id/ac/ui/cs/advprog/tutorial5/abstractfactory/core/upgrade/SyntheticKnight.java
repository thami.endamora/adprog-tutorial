package id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.Armory;

public class SyntheticKnight extends Knight {

    public SyntheticKnight(Armory armory) {
        this.armory = armory;
    }

    @Override
    public void prepare() {
        this.name = "Synthetic Knight";
        this.weapon = this.armory.craftWeapon();
        this.skill = this.armory.learnSkill();
    }
}
