package id.ac.ui.cs.advprog.tutorial5.abstractfactory.repository;

import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.KnightAcademy;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AcademyRepository {

    private Map<String, KnightAcademy> knightAcademies = new HashMap<>();

    public List<KnightAcademy> getKnightAcademies() {
        return new ArrayList<>(knightAcademies.values());
    }

    public KnightAcademy getKnightAcademyByName(String academyName) {
        return knightAcademies.get(academyName);
    }

    public void addKnightAcademy(String academyName, KnightAcademy knightAcademy) {
        knightAcademies.put(academyName,knightAcademy);
    }
}
