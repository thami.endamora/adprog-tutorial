package id.ac.ui.cs.advprog.tutorial5.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.upgrade.Knight;

import java.util.List;

public interface AcademyService {

    void produceKnight(String academyName, String knightType);
    List<KnightAcademy> getKnightAcademies();
    Knight getKnight();
}
