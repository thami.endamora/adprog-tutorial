package id.ac.ui.cs.advprog.tutorial5.singleton.controller;

import id.ac.ui.cs.advprog.tutorial5.singleton.core.HolyWish;
import id.ac.ui.cs.advprog.tutorial5.singleton.service.HolyGrail;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/holy-grail")
public class HolyGrailController {

    private final HolyGrail holyGrail;

    public HolyGrailController(HolyGrail holyGrail) {
        this.holyGrail = holyGrail;
    }

    @GetMapping("/")
    public String holyGrailHome(Model model) {
        HolyWish holyWish = holyGrail.getHolyWish();
        model.addAttribute("wish", holyWish);
        return "singleton/holy-grail";
    }

    @PostMapping("/make-a-wish")
    public String makeAWish(@RequestParam(value = "wish") String wish) {
        holyGrail.makeAWish(wish);
        return "redirect:/holy-grail/";
    }
}
