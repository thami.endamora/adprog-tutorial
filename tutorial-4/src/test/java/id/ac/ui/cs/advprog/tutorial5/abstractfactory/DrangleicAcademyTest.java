package id.ac.ui.cs.advprog.tutorial5.abstractfactory;

import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        this.drangleicAcademy = new DrangleicAcademy();
        this.majesticKnight = this.drangleicAcademy.getKnight("majestic");
        this.metalClusterKnight = this.drangleicAcademy.getKnight("metal cluster");
        this.syntheticKnight = this.drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        assertTrue(this.majesticKnight instanceof MajesticKnight);
        assertTrue(this.metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(this.syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        assertEquals("Majestic Knight",this.majesticKnight.getName());
        assertEquals("Metal Cluster",this.metalClusterKnight.getName());
        assertEquals("Synthetic Knight",this.syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        assertEquals("Shining Armor",this.majesticKnight.getArmor().getName());
        assertEquals("Shining Buster",this.majesticKnight.getWeapon().getName());

        assertEquals("Shining Armor",this.metalClusterKnight.getArmor().getName());
        assertEquals("Shining Force",this.metalClusterKnight.getSkill().getName());

        assertEquals("Shining Buster",this.syntheticKnight.getWeapon().getName());
        assertEquals("Shining Force",this.syntheticKnight.getSkill().getName());
    }

}
