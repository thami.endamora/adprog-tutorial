package id.ac.ui.cs.advprog.tutorial5.abstractfactory.armory.armor;

import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.armor.MetalArmor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MetalArmorTest {

    Armor metalArmor;

    @BeforeEach
    public void setUp(){
        metalArmor = new MetalArmor();
    }

    @Test
    public void testToString(){
        assertEquals("Metal Armor",this.metalArmor.getName());
    }

    @Test
    public void testDescription(){
        assertEquals("Metal!",this.metalArmor.getDescription());
    }
}
