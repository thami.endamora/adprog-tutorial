package id.ac.ui.cs.advprog.tutorial5.abstractfactory.armory.armor;

import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.armor.ShiningArmor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShiningArmorTest {

    Armor shiningArmor;

    @BeforeEach
    public void setUp(){
        shiningArmor = new ShiningArmor();
    }

    @Test
    public void testToString(){
        assertEquals("Shining Armor",this.shiningArmor.getName());
    }

    @Test
    public void testDescription(){
        assertEquals("Shine!",this.shiningArmor.getDescription());
    }
}
