package id.ac.ui.cs.advprog.tutorial5.abstractfactory.armory.skill;

import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial5.abstractfactory.core.armory.skill.Skill;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ThousandYearsOfPainTest {

    Skill thousandYearsOfPain;

    @BeforeEach
    public void setUp(){
        thousandYearsOfPain = new ThousandYearsOfPain();
    }

    @Test
    public void testToString(){
        assertEquals("Thousand Years of Pain",this.thousandYearsOfPain.getName());
    }

    @Test
    public void testDescription(){
        assertEquals("Pain!",this.thousandYearsOfPain.getDescription());
    }
}
