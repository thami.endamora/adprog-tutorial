package id.ac.ui.cs.advprog.tutorial5.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial5.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    @Test
    public void testService(){
        academyService = new AcademyServiceImpl(new AcademyRepository());
        academyService.produceKnight("Drangleic","majestic");
        assertEquals("Majestic Knight",academyService.getKnight().getName());

        academyService.produceKnight("Lordran","synthetic");
        assertEquals("Synthetic Knight",academyService.getKnight().getName());

        assertEquals(2,academyService.getKnightAcademies().size());
    }
}
