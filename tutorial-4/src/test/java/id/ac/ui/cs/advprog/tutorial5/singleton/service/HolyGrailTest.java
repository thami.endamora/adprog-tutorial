package id.ac.ui.cs.advprog.tutorial5.singleton.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    @Test
    public void testHollyGrail(){
        HolyGrail holyGrail = new HolyGrail();
        assertNotNull(holyGrail.getHolyWish());

        holyGrail.makeAWish("This is a wish");
        assertEquals("This is a wish",holyGrail.getHolyWish().getWish());
    }
}
