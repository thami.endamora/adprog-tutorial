package id.ac.ui.cs.advprog.tutorial5.controller;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/soul")
public class SoulController {

    @Autowired
    private final SoulService soulService;

    public SoulController(SoulService soulService){
        this.soulService = soulService;
    }

    @GetMapping
    public ResponseEntity<List<Soul>> findAll() {
        List<Soul> soulList = soulService.findAll();
        return new ResponseEntity<>(soulList, HttpStatus.OK);
        // TODO: Use service to complete me.
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Soul soul) {
        soulService.register(soul);
        return new ResponseEntity(HttpStatus.OK);
        // TODO: Use service to complete me.
    }

    @GetMapping("/{id}")
    public ResponseEntity<Soul> findById(@PathVariable Long id) {
        Optional<Soul> soul = soulService.findSoul(id);
        if (soul.isPresent()){
            return new ResponseEntity<>(soul.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        // TODO: Use service to complete me.
    }

    @PutMapping("/{id}")
    public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul) {
        Soul update = soulService.rewrite(soul);
        return new ResponseEntity<>(update, HttpStatus.OK);
        // TODO: Use service to complete me.
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        soulService.erase(id);
        return new ResponseEntity(HttpStatus.OK);
        // TODO: Use service to complete me.
    }
}

