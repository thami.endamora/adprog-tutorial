package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SoulServiceImpl implements SoulService {

    @Autowired
    private SoulRepository repository;

    public SoulServiceImpl(SoulRepository soulRepository){
        this.repository = soulRepository;
    }

    public List<Soul> findAll(){
        return repository.findAll();
    }

    public Optional<Soul> findSoul(Long id){
        return repository.findById(id);
    }

    public void erase(Long id){
        //delete
        repository.deleteById(id);
    }

    public Soul rewrite(Soul soul){
        //update
        return repository.save(soul);
    }
    public Soul register(Soul soul){
        //create
        repository.save(soul);
        return soul;
    }
    // TODO: implementasi semua method di SoulService.java. Coba lihat dokumentasi JpaRepository untuk mengimplementasikan Service
}

