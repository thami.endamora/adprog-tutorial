import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SoulTest {

    private Soul soul;

    @BeforeEach
    public void setUp() {
        soul = new Soul("Thami",19,"F","Student");
        soul.setId(0);
    }

    @Test
    void getGender() {
        assertEquals("F",soul.getGender());
    }

    @Test
    void setGender() {
        soul.setGender("Gender");
        assertEquals("Gender",soul.getGender());
    }

    @Test
    void getOccupation() {
        assertEquals("Student",soul.getOccupation());
    }

    @Test
    void setOccupation() {
        soul.setOccupation("Occupation");
        assertEquals("Occupation",soul.getOccupation());
    }

    @Test
    void getName() {
        assertEquals("Thami",soul.getName());
    }

    @Test
    void setName() {
        soul.setName("Name");
        assertEquals("Name",soul.getName());
    }

    @Test
    void getAge() {
        assertEquals(19,soul.getAge());
    }

    @Test
    void setAge() {
        soul.setAge(1);
        assertEquals(1,soul.getAge());
    }

    @Test
    void getId() {
        assertEquals(0,soul.getId());
    }

    @Test
    void setId() {
        soul.setId(1);
        assertEquals(1,soul.getId());
    }

}