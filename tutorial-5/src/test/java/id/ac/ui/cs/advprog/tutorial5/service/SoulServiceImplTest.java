import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import id.ac.ui.cs.advprog.tutorial5.service.SoulServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class SoulServiceImplTest {
    @Mock
    private SoulRepository soulRepository;

    @InjectMocks
    private SoulServiceImpl soulService = new SoulServiceImpl(soulRepository);

    private Soul soul;

    @BeforeEach
    public void setUp(){
        soul = new Soul("Thami",19,"F","Student");
        soul.setId(1);
    }

    @Test
    void testFindAll() {
        List<Soul> souls = soulRepository.findAll();
        lenient().when(soulService.findAll()).thenReturn(souls);
    }

    @Test
    void testFindSoul() {
        Optional<Soul> notFound = soulService.findSoul((long)1000);
        Optional<Soul> found = soulService.findSoul((long)1);
        assertEquals(null, notFound.orElse(null));
        lenient().when(soulService.findSoul((long)1000)).thenReturn(Optional.empty());
        lenient().when(soulService.findSoul((long)1)).thenReturn(Optional.of(soul));
    }

    @Test
    void testErase() {
        soulService.erase((long)1);
        lenient().when(soulService.findSoul((long)1)).thenReturn(Optional.empty());
    }

    @Test
    void testRewrite() {
        soul.setName("New");
        soulService.rewrite(soul);
        lenient().when(soulService.findSoul((long)1)).thenReturn(Optional.of(soul));
    }

    @Test
    void testRegister() {
        Soul soul = new Soul("Test",1,"F","Something");
        soul.setId(100);
        soulService.register(soul);
        lenient().when(soulService.findSoul((long)100)).thenReturn(Optional.of(soul));
        lenient().when(soulService.findSoul((long)500)).thenReturn(Optional.of(soul));
    }
}
